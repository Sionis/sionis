$(document).ready(function(){
    
    (function($) {
        "use strict";

    
    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "type the correct answer -_-");

    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                subject: {
                    required: true,
                    minlength: 4
                },
                number: {
                    required: true,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true,
                    minlength: 20
                }
            },
            messages: {
                name: {
                    required: "Ну и как мне обращаться к тебе если ты не оставил своего имени?",
                    minlength: "Напишите хотя бы 2 буквы"
                },
                subject: {
                    required: "Серьезно? Ты не знаешь что за тема?",
                    minlength: "Тема должна состоять не менее чем из 4-ч символов"
                },
                number: {
                    required: "Да ладно, У тебя нет мобильного?",
                    minlength: "Минимальная длина номера телефона 5 символов"
                },
                email: {
                    required: "нет email, нет сообщения"
                },
                message: {
                    required: "Ммм. да... Напиши хоть кем ты работаешь если основное сообщение такое короткое",
                    minlength: "Это все? Серьезно?"
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    data: $(form).serialize(),
                    url:"contact_process.php",
                    success: function() {
                        $('#contactForm :input').attr('disabled', 'disabled');
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor','default');
                            $('#success').fadeIn()
                            $('.modal').modal('hide');
		                	$('#success').modal('show');
                        })
                    },
                    error: function() {
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $('#error').fadeIn()
                            $('.modal').modal('hide');
		                	$('#error').modal('show');
                        })
                    }
                })
            }
        })
    })
        
 })(jQuery)
})